"""Models returned from FB Api
"""

import json

from dataclasses import dataclass
from typing import Any, ClassVar, Dict, Iterable, List, Optional, Protocol, Tuple, Type, Union
from facebook_business.adobjects.ad import Ad

from facebook_business.adobjects.adaccount import AdAccount
from facebook_business.adobjects.campaign import Campaign

Primitive = Union[str, int]

# pylint: disable=too-few-public-methods
class Dataclass(Protocol):
    """A dataclass protocol for typehinting"""

    __dataclass_fields__: ClassVar[dict]
    __filename__: ClassVar[str]


def get_fields(model: Type[Dataclass], *, no_auto: bool = False) -> List:
    """Get list of fields for specified dataclass"""
    fields = getattr(model, "__fields__", [])
    return fields or [
        field
        for field in model.__dataclass_fields__
        if not(field.startswith("_")
        or field in ["instance", "InsightsModel"]
        or (no_auto and field in getattr(model, "__auto_fields__", [])))
    ]


def clean_field(field: Any) -> Optional[Primitive]:
    """Always return the most sensible primitive"""
    if field is None:
        return None
    if isinstance(field, (str, int)):
        return field
    if isinstance(field, (List, Dict)):
        return json.dumps(field, default=str)
    if hasattr(field, "_json"):
        return json.dumps(field._json, default=str)  # pylint: disable=protected-access
    return str(field)


class ApiCreate:
    """Mixin to build from complex structures"""

    __dataclass_fields__: ClassVar[dict]
    __filename__: ClassVar[str]
    __fields__: Iterable[str]
    __auto_fields__: ClassVar[Iterable[str]]  # fields retrieved automatically, cannot be explicitly asked for

    __complex_fields__: Optional[Dict[str, Union[Tuple[str, ...], List[str]]]] = None
    InsightsModel: ClassVar[Type["InsightsModel"]]

    def __init__(self, *args, **kwargs):
        raise NotImplementedError("This class cannot be initialized")

    @classmethod
    def from_api(cls, response: Dict[str, Any]) -> Any:
        """Create from API response"""
        complex_details = {}
        complex_fields = getattr(cls, "__complex_fields__", {}) or {}
        for root_field, subfields in complex_fields.items():
            complex_details.update(
                {f"{root_field}_{subfield}": response[root_field][subfield] for subfield in subfields}
            )
        return cls(
            **{field: clean_field(response.get(field)) for field in get_fields(cls) if not field in complex_fields},
            **complex_details,
        )


@dataclass
class InsightsModel(ApiCreate):
    """Insights"""

    __dataclass_fields__: ClassVar[dict]
    __filename__: ClassVar = "insights.csv"
    __auto_fields__: ClassVar[Iterable[str]] = [
        "action_device",
        "action_canvas_component_name",
        "action_carousel_card_id",
        "action_carousel_card_name",
        "action_destination",
        "action_reaction",
        "action_target_id",
        "action_type",
        "action_video_sound",
        "action_video_type",
        "ad_format_asset",
        "age",
        "app_id",
        "body_asset",
        "call_to_action_asset",
        "country",
        "description_asset",
        "device_platform",
        "dma",
        "frequency_value",
        "gender",
        "hourly_stats_aggregated_by_advertiser_time_zone",
        "hourly_stats_aggregated_by_audience_time_zone",
        "image_asset",
        "impression_device",
        "is_conversion_id_modeled",
        "link_url_asset",
        "place_page_id",
        "platform_position",
        "product_id",
        "publisher_platform",
        "region",
        "skan_campaign_id",
        "skan_conversion_id",
        "title_asset",
        "user_segment_key",
        "video_asset",
    ]

    account_currency: Optional[Any] = None
    account_id: Optional[Any] = None
    account_name: Optional[Any] = None
    action_values: Optional[Any] = None
    actions: Optional[Any] = None
    ad_bid_value: Optional[Any] = None
    ad_click_actions: Optional[Any] = None
    ad_id: Optional[Any] = None
    ad_impression_actions: Optional[Any] = None
    ad_name: Optional[Any] = None
    adset_bid_value: Optional[Any] = None
    adset_end: Optional[Any] = None
    adset_id: Optional[Any] = None
    adset_name: Optional[Any] = None
    adset_start: Optional[Any] = None
    age_targeting: Optional[Any] = None
    attribution_setting: Optional[Any] = None
    auction_bid: Optional[Any] = None
    auction_competitiveness: Optional[Any] = None
    auction_max_competitor_bid: Optional[Any] = None
    buying_type: Optional[Any] = None
    campaign_id: Optional[Any] = None
    campaign_name: Optional[Any] = None
    canvas_avg_view_percent: Optional[Any] = None
    canvas_avg_view_time: Optional[Any] = None
    catalog_segment_actions: Optional[Any] = None
    catalog_segment_value: Optional[Any] = None
    catalog_segment_value_mobile_purchase_roas: Optional[Any] = None
    catalog_segment_value_omni_purchase_roas: Optional[Any] = None
    catalog_segment_value_website_purchase_roas: Optional[Any] = None
    clicks: Optional[Any] = None
    conversion_rate_ranking: Optional[Any] = None
    conversion_values: Optional[Any] = None
    conversions: Optional[Any] = None
    converted_product_quantity: Optional[Any] = None
    converted_product_value: Optional[Any] = None
    cost_per_15_sec_video_view: Optional[Any] = None
    cost_per_2_sec_continuous_video_view: Optional[Any] = None
    cost_per_action_type: Optional[Any] = None
    cost_per_ad_click: Optional[Any] = None
    cost_per_conversion: Optional[Any] = None
    cost_per_dda_countby_convs: Optional[Any] = None
    cost_per_estimated_ad_recallers: Optional[Any] = None
    cost_per_inline_link_click: Optional[Any] = None
    cost_per_inline_post_engagement: Optional[Any] = None
    cost_per_one_thousand_ad_impression: Optional[Any] = None
    cost_per_outbound_click: Optional[Any] = None
    cost_per_thruplay: Optional[Any] = None
    cost_per_unique_action_type: Optional[Any] = None
    cost_per_unique_click: Optional[Any] = None
    # cost_per_unique_conversion: Optional[Any] = None
    cost_per_unique_inline_link_click: Optional[Any] = None
    cost_per_unique_outbound_click: Optional[Any] = None
    cpc: Optional[Any] = None
    cpm: Optional[Any] = None
    cpp: Optional[Any] = None
    created_time: Optional[Any] = None
    ctr: Optional[Any] = None
    date_start: Optional[Any] = None
    date_stop: Optional[Any] = None
    dda_countby_convs: Optional[Any] = None
    dda_results: Optional[Any] = None
    engagement_rate_ranking: Optional[Any] = None
    estimated_ad_recall_rate: Optional[Any] = None
    estimated_ad_recall_rate_lower_bound: Optional[Any] = None
    estimated_ad_recall_rate_upper_bound: Optional[Any] = None
    estimated_ad_recallers: Optional[Any] = None
    estimated_ad_recallers_lower_bound: Optional[Any] = None
    estimated_ad_recallers_upper_bound: Optional[Any] = None
    frequency: Optional[Any] = None
    full_view_impressions: Optional[Any] = None
    full_view_reach: Optional[Any] = None
    gender_targeting: Optional[Any] = None
    impressions: Optional[Any] = None
    inline_link_click_ctr: Optional[Any] = None
    inline_link_clicks: Optional[Any] = None
    inline_post_engagement: Optional[Any] = None
    instant_experience_clicks_to_open: Optional[Any] = None
    instant_experience_clicks_to_start: Optional[Any] = None
    instant_experience_outbound_clicks: Optional[Any] = None
    interactive_component_tap: Optional[Any] = None
    labels: Optional[Any] = None
    location: Optional[Any] = None
    mobile_app_purchase_roas: Optional[Any] = None
    objective: Optional[Any] = None
    optimization_goal: Optional[Any] = None
    outbound_clicks: Optional[Any] = None
    outbound_clicks_ctr: Optional[Any] = None
    place_page_name: Optional[Any] = None
    purchase_roas: Optional[Any] = None
    qualifying_question_qualify_answer_rate: Optional[Any] = None
    quality_ranking: Optional[Any] = None
    quality_score_ectr: Optional[Any] = None
    quality_score_ecvr: Optional[Any] = None
    quality_score_organic: Optional[Any] = None
    reach: Optional[Any] = None
    social_spend: Optional[Any] = None
    spend: Optional[Any] = None
    unique_actions: Optional[Any] = None
    unique_clicks: Optional[Any] = None
    unique_ctr: Optional[Any] = None
    unique_inline_link_click_ctr: Optional[Any] = None
    unique_inline_link_clicks: Optional[Any] = None
    unique_link_clicks_ctr: Optional[Any] = None
    unique_outbound_clicks: Optional[Any] = None
    unique_outbound_clicks_ctr: Optional[Any] = None
    unique_video_continuous_2_sec_watched_actions: Optional[Any] = None
    unique_video_view_15_sec: Optional[Any] = None
    updated_time: Optional[Any] = None
    video_15_sec_watched_actions: Optional[Any] = None
    video_30_sec_watched_actions: Optional[Any] = None
    video_avg_time_watched_actions: Optional[Any] = None
    video_continuous_2_sec_watched_actions: Optional[Any] = None
    video_p100_watched_actions: Optional[Any] = None
    video_p25_watched_actions: Optional[Any] = None
    video_p50_watched_actions: Optional[Any] = None
    video_p75_watched_actions: Optional[Any] = None
    video_p95_watched_actions: Optional[Any] = None
    video_play_actions: Optional[Any] = None
    video_play_curve_actions: Optional[Any] = None
    video_play_retention_0_to_15s_actions: Optional[Any] = None
    video_play_retention_20_to_60s_actions: Optional[Any] = None
    video_play_retention_graph_actions: Optional[Any] = None
    video_thruplay_watched_actions: Optional[Any] = None
    video_time_watched_actions: Optional[Any] = None
    website_ctr: Optional[Any] = None
    website_purchase_roas: Optional[Any] = None
    wish_bid: Optional[Any] = None

    # Breakdown fields
    action_device: Optional[Any] = None
    action_canvas_component_name: Optional[Any] = None
    action_carousel_card_id: Optional[Any] = None
    action_carousel_card_name: Optional[Any] = None
    action_destination: Optional[Any] = None
    action_reaction: Optional[Any] = None
    action_target_id: Optional[Any] = None
    action_type: Optional[Any] = None
    action_video_sound: Optional[Any] = None
    action_video_type: Optional[Any] = None
    ad_format_asset: Optional[Any] = None
    age: Optional[Any] = None
    app_id: Optional[Any] = None
    body_asset: Optional[Any] = None
    call_to_action_asset: Optional[Any] = None
    country: Optional[Any] = None
    description_asset: Optional[Any] = None
    device_platform: Optional[Any] = None
    dma: Optional[Any] = None
    frequency_value: Optional[Any] = None
    gender: Optional[Any] = None
    hourly_stats_aggregated_by_advertiser_time_zone: Optional[Any] = None
    hourly_stats_aggregated_by_audience_time_zone: Optional[Any] = None
    image_asset: Optional[Any] = None
    impression_device: Optional[Any] = None
    is_conversion_id_modeled: Optional[Any] = None
    link_url_asset: Optional[Any] = None
    place_page_id: Optional[Any] = None
    platform_position: Optional[Any] = None
    product_id: Optional[Any] = None
    publisher_platform: Optional[Any] = None
    region: Optional[Any] = None
    skan_campaign_id: Optional[Any] = None
    skan_conversion_id: Optional[Any] = None
    title_asset: Optional[Any] = None
    user_segment_key: Optional[Any] = None
    video_asset: Optional[Any] = None


class CampaignInsightsModel(InsightsModel):
    """Campaign insights"""

    __filename__: ClassVar = "campaign_insights.csv"


class AdInsightsModel(InsightsModel):
    """Ad insights"""

    __filename__: ClassVar = "ad_insights.csv"


class AccountInsightsModel(InsightsModel):
    """Account insights"""

    __filename__: ClassVar = "account_insights.csv"


@dataclass
class AdAccountModel(ApiCreate):
    """AdAccount"""

    __dataclass_fields__: ClassVar[dict]
    __filename__: ClassVar = "adaccounts.csv"

    InsightsModel: ClassVar[Type[InsightsModel]] = AccountInsightsModel

    instance: AdAccount
    account_id: Optional[Any] = None
    created_time: Optional[Any] = None
    fb_entity: Optional[Any] = None
    id: Optional[Any] = None
    media_agency: Optional[Any] = None
    name: Optional[Any] = None
    partner: Optional[Any] = None


@dataclass
class CampaignModel(ApiCreate):
    """Campaign"""

    __dataclass_fields__: ClassVar[dict]
    __filename__: ClassVar = "campaigns.csv"

    InsightsModel: ClassVar[Type[InsightsModel]] = CampaignInsightsModel

    account_id: Optional[Any] = None
    ad_strategy_id: Optional[Any] = None
    adbatch: Optional[Any] = None
    adlabels: Optional[Any] = None
    bid_strategy: Optional[Any] = None
    boosted_object_id: Optional[Any] = None
    brand_lift_studies: Optional[Any] = None
    budget_rebalance_flag: Optional[Any] = None
    budget_remaining: Optional[Any] = None
    buying_type: Optional[Any] = None
    can_create_brand_lift_study: Optional[Any] = None
    can_use_spend_cap: Optional[Any] = None
    configured_status: Optional[Any] = None
    created_time: Optional[Any] = None
    daily_budget: Optional[Any] = None
    effective_status: Optional[Any] = None
    execution_options: Optional[Any] = None
    id: Optional[Any] = None
    is_skadnetwork_attribution: Optional[Any] = None
    issues_info: Optional[Any] = None
    iterative_split_test_configs: Optional[Any] = None
    last_budget_toggling_time: Optional[Any] = None
    lifetime_budget: Optional[Any] = None
    name: Optional[Any] = None
    objective: Optional[Any] = None
    pacing_type: Optional[Any] = None
    promoted_object: Optional[Any] = None
    recommendations: Optional[Any] = None
    smart_promotion_type: Optional[Any] = None
    source_campaign_id: Optional[Any] = None
    special_ad_categories: Optional[Any] = None
    special_ad_category: Optional[Any] = None
    special_ad_category_country: Optional[Any] = None
    spend_cap: Optional[Any] = None
    start_time: Optional[Any] = None
    status: Optional[Any] = None
    stop_time: Optional[Any] = None
    topline_id: Optional[Any] = None
    updated_time: Optional[Any] = None
    upstream_events: Optional[Any] = None


@dataclass
class InvoiceModel(ApiCreate):
    """Invoice (OmegaCustomerTrx)"""

    __dataclass_fields__: ClassVar[dict]
    __filename__: ClassVar = "invoices.csv"

    __fields__ = [
        "ad_account_ids",
        "advertiser_name",
        "amount",
        "amount_due",
        "billed_amount_details",
        "billing_period",
        "cdn_download_uri",
        "currency",
        "download_uri",
        "due_date",
        "entity",
        "id",
        "invoice_date",
        "invoice_id",
        "invoice_type",
        "liability_type",
        "payment_status",
        "payment_term",
        "type",
    ]
    __complex_fields__ = {
        "amount_due": ("amount", "currency", "amount_in_hundredths"),
        "billed_amount_details": ("net_amount", "tax_amount", "currency", "total_amount"),
    }

    ad_account_ids: Optional[Any] = None
    advertiser_name: Optional[Any] = None
    amount: Optional[Any] = None
    amount_due_amount: Optional[Any] = None
    amount_due_currency: Optional[Any] = None
    amount_due_amount_in_hundredths: Optional[Any] = None
    billed_amount_details_net_amount: Optional[Any] = None
    billed_amount_details_tax_amount: Optional[Any] = None
    billed_amount_details_currency: Optional[Any] = None
    billed_amount_details_total_amount: Optional[Any] = None
    billing_period: Optional[Any] = None
    cdn_download_uri: Optional[Any] = None
    currency: Optional[Any] = None
    download_uri: Optional[Any] = None
    due_date: Optional[Any] = None
    entity: Optional[Any] = None
    id: Optional[Any] = None
    invoice_date: Optional[Any] = None
    invoice_id: Optional[Any] = None
    invoice_type: Optional[Any] = None
    liability_type: Optional[Any] = None
    payment_status: Optional[Any] = None
    payment_term: Optional[Any] = None
    type: Optional[Any] = None


@dataclass
class AdModel(ApiCreate):
    """Ad"""

    __dataclass_fields__: ClassVar[dict]
    __filename__: ClassVar = "ads.csv"

    InsightsModel: ClassVar[Type[InsightsModel]] = AdInsightsModel

    account_id: Optional[Any] = None
    adlabels: Optional[Any] = None
    adset_id: Optional[Any] = None
    bid_amount: Optional[Any] = None
    bid_info: Optional[Any] = None
    bid_type: Optional[Any] = None
    campaign_id: Optional[Any] = None
    configured_status: Optional[Any] = None
    conversion_domain: Optional[Any] = None
    conversion_specs: Optional[Any] = None
    created_time: Optional[Any] = None
    demolink_hash: Optional[Any] = None
    display_sequence: Optional[Any] = None
    effective_status: Optional[Any] = None
    engagement_audience: Optional[Any] = None
    failed_delivery_checks: Optional[Any] = None
    id: Optional[Any] = None
    issues_info: Optional[Any] = None
    last_updated_by_app_id: Optional[Any] = None
    name: Optional[Any] = None
    source_ad_id: Optional[Any] = None
    status: Optional[Any] = None
    targeting: Optional[Any] = None
    updated_time: Optional[Any] = None


@dataclass
class AdsPixelModel(ApiCreate):
    """AdsPixel"""

    __dataclass_fields__: ClassVar[dict]
    __filename__: ClassVar = "adspixels.csv"

    account_id: Optional[Any] = None
    automatic_matching_fields: Optional[Any] = None
    can_proxy: Optional[Any] = None
    code: Optional[Any] = None
    creation_time: Optional[Any] = None
    data_use_setting: Optional[Any] = None
    enable_automatic_matching: Optional[Any] = None
    first_party_cookie_status: Optional[Any] = None
    id: Optional[Any] = None
    is_created_by_business: Optional[Any] = None
    is_crm: Optional[Any] = None
    is_unavailable: Optional[Any] = None
    last_fired_time: Optional[Any] = None
    name: Optional[Any] = None


insights_map = {Ad: AdInsightsModel, Campaign: CampaignInsightsModel, AdAccount: AccountInsightsModel}
