"""Facebook Business Client
"""

import logging

from functools import cached_property
from typing import Any, Callable, Dict, Iterable, Optional, Tuple, Type, Union, List

import backoff
from facebook_business.api import FacebookAdsApi, Cursor
from facebook_business.adobjects.user import User
from facebook_business.adobjects.business import Business
from facebook_business.adobjects.campaign import Campaign
from facebook_business.adobjects.ad import Ad
from facebook_business.adobjects.adaccount import AdAccount
from facebook_business.adobjects.omegacustomertrx import OmegaCustomerTrx
from facebook_business.adobjects.adspixel import AdsPixel
from facebook_business.exceptions import FacebookRequestError


from fbusiness.models import (
    AccountInsightsModel,
    AdAccountModel,
    AdInsightsModel,
    AdModel,
    AdsPixelModel,
    CampaignInsightsModel,
    CampaignModel,
    Dataclass,
    InsightsModel,
    InvoiceModel,
    get_fields,
    insights_map,
)

logger = logging.getLogger(__name__)


def backoff_log(details):
    """Log details when backing off"""
    logger.info(
        "Backing off %d seconds after %d tries calling function %s",
        details["wait"],
        details["tries"],
        details["target"],
    )


class RetryableCursor:
    """Cursor that gives until there is nothing to be given anymore and waits for the API limits to reset if needed"""

    def __init__(self, cursor: Cursor):
        """Create RetryableCursor on top of standard FB Manager API Cursor object"""
        self.cursor = cursor

    def __iter__(self) -> "RetryableCursor":
        return self

    @backoff.on_exception(
        backoff.constant, exception=FacebookRequestError, max_time=3600, on_backoff=backoff_log, interval=300
    )
    def __next__(self) -> Any:
        item = self.cursor.get_one()
        if item is None:
            raise StopIteration()
        return item


class FBClient:
    """Facebook Business client"""

    def __init__(
        self,
        app_id: str,
        app_secret: str,
        access_token: str,
        advertiser_id: Optional[str] = None,
        account_id: Optional[str] = None,
    ):
        self.app_id = app_id
        self.app_secret = app_secret
        self.access_token = access_token
        self.advertiser_id = advertiser_id
        self.account_id = account_id
        FacebookAdsApi.init(
            app_id=self.app_id,
            app_secret=self.app_secret,
            access_token=self.access_token,
        )
        self._mapping: Dict[Type[Dataclass], Callable[..., Iterable[Dataclass]]] = {
            AdModel: self.iter_advertiser_ads,
            AdsPixelModel: self.iter_advertiser_adpixels,
            CampaignModel: self.iter_campaigns,
            InvoiceModel: self.iter_advertiser_invoices,
        }

    @cached_property
    @backoff.on_exception(backoff.constant, FacebookRequestError, max_time=3600, on_backoff=backoff_log, interval=300)
    def user(self) -> User:
        """Get myself"""
        return User(fbid="me")

    @cached_property
    @backoff.on_exception(backoff.constant, FacebookRequestError, max_time=3600, on_backoff=backoff_log, interval=300)
    def account(self) -> AdAccountModel:
        """Get account associated with advertiser"""
        if self.advertiser_id:
            return self._get_account_by_advertiser(self.advertiser_id)
        if self.account_id:
            return self._get_account_by_id(self.account_id)
        raise ValueError("Neither advertiser_id nor account_id was set")

    @backoff.on_exception(backoff.constant, FacebookRequestError, max_time=3600, on_backoff=backoff_log, interval=300)
    def list_accounts(self) -> Iterable[AdAccountModel]:
        """List accounts"""
        logger.info("Listing available business accounts")
        return [
            AdAccountModel(instance=account, **account)
            for account in self.user.get_ad_accounts(fields=get_fields(AdAccountModel))
        ]

    @backoff.on_exception(backoff.constant, FacebookRequestError, max_time=3600, on_backoff=backoff_log, interval=300)
    def iter_campaigns(self, params: Optional[Dict[str, str]] = None) -> Iterable[Tuple[CampaignModel, Campaign]]:
        """List campaigns for ad account as a tuple (saveamble-model, instance)"""
        return (
            (CampaignModel.from_api(campaign), campaign)
            for campaign in RetryableCursor(
                self.account.instance.get_campaigns(fields=get_fields(CampaignModel), params=params)
            )
        )

    def _get_account_by_advertiser(self, advertiser_id: str) -> AdAccountModel:
        """Get account by advertiser id"""
        for account in self.list_accounts():
            if account.end_advertiser == advertiser_id:
                return account
        raise ValueError(f"Advertiser with ID {advertiser_id} was not found")

    def _get_account_by_id(self, account_id: str) -> AdAccountModel:
        """Get account by account id"""
        for account in self.list_accounts():
            if account_id in (account.id, account.account_id):
                return account
        raise ValueError(f"Advertiser with account ID {account_id} was not found")

    @backoff.on_exception(backoff.constant, FacebookRequestError, max_time=3600, on_backoff=backoff_log, interval=300)
    def iter_advertiser_invoices(
        self, params: Optional[Dict[str, str]] = None
    ) -> Iterable[Tuple[InvoiceModel, OmegaCustomerTrx]]:
        """Get invoices for an advertiser"""
        if not self.advertiser_id:
            raise ValueError("Cannot iter invoices without advertiser_id")
        business = Business(self.advertiser_id)
        return (
            (InvoiceModel.from_api(trx), trx)
            for trx in RetryableCursor(business.get_business_invoices(fields=get_fields(InvoiceModel), params=params))
        )

    @backoff.on_exception(backoff.constant, FacebookRequestError, max_time=3600, on_backoff=backoff_log, interval=300)
    def iter_advertiser_ads(self, params: Optional[Dict[str, str]] = None) -> Iterable[Tuple[AdModel, Ad]]:
        """Get all Ads by an advertiser"""
        return (
            (AdModel.from_api(ad), ad)
            for ad in RetryableCursor(self.account.instance.get_ads(fields=get_fields(AdModel), params=params))
        )

    @backoff.on_exception(backoff.constant, FacebookRequestError, max_time=3600, on_backoff=backoff_log, interval=300)
    def iter_advertiser_adpixels(
        self, params: Optional[Dict[str, str]] = None
    ) -> Iterable[Tuple[AdsPixelModel, AdsPixel]]:
        """Get all ads pixels"""
        return (
            (AdsPixelModel.from_api(apixel), apixel)
            for apixel in RetryableCursor(
                self.account.instance.get_ads_pixels(fields=get_fields(AdsPixelModel), params=params)
            )
        )

    def list_by_model(
        self, model: Type[Dataclass], **kwargs
    ) -> Iterable[Tuple[Dataclass, Union[Ad, OmegaCustomerTrx, AdsPixel, Campaign]]]:
        """return list of data by the model class"""
        return self._mapping[model](**kwargs)

    @backoff.on_exception(backoff.constant, FacebookRequestError, max_time=3600, on_backoff=backoff_log, interval=300)
    def list_insights(
        self,
        base: Union[Ad, Campaign, AdAccount],
        date_preset: str,
        time_increment: int,
        limit: int = 500,
        breakdown: Optional[List[str]] = None,
    ) -> Iterable[Union[AdInsightsModel, CampaignInsightsModel, AccountInsightsModel]]:
        """Return list of insights for a specific {base} instance"""
        cls = insights_map[type(base)]
        return (
            cls.from_api(line)
            for line in RetryableCursor(
                base.get_insights(
                    fields=get_fields(InsightsModel, no_auto=True),
                    params={
                        "date_preset": date_preset,
                        "time_increment": time_increment,
                        "limit": limit,
                        "breakdowns": breakdown,
                    },
                )
            )
        )
