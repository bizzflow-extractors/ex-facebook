"""Facebook Business Extractor
"""

import os
import logging
from typing import Dict, List, Type
from bizztreat_base.config import Config
from bizztreat_base.utils.writer import ModelWriter

from fbusiness.client import FBClient
from fbusiness.models import AdModel, AdsPixelModel, ApiCreate, CampaignModel, InvoiceModel, InsightsModel

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

object_map: Dict[str, Type[ApiCreate]] = {
    "campaigns": CampaignModel,
    "ads": AdModel,
    "pixels": AdsPixelModel,
    "invoices": InvoiceModel,
}


def main():
    """Main entrypoint"""
    proxy = Config(force_schema=True)
    if not os.path.exists(proxy.args.output):
        os.makedirs(proxy.args.output)

    logger.info("Initializing Facebook Business API")

    client = FBClient(
        app_id=proxy.config["app_id"],
        app_secret=proxy.config["app_secret"],
        access_token=proxy.config["access_token"],
        advertiser_id=proxy.config.get("advertiser_id"),
        account_id=proxy.config.get("account_id"),
    )

    models: List[Type[ApiCreate]] = []
    model_config: Dict[Type[ApiCreate], Dict[str, Dict]] = {}
    model_insights_config: Dict[Type[ApiCreate], Dict[str, Dict]] = {}

    limit = proxy.config.get("limit") or 500

    for endpoint in proxy.config["objects"]:
        if isinstance(endpoint, str):
            model = object_map[endpoint]
            models.append(model)
            model_insights_config[model] = {}
            model_config[model] = {}
            continue
        model = object_map[endpoint["type"]]
        model_config[model] = endpoint
        model_insights_config[model] = {**endpoint.get("insights", {})}
        models.append(model)
        if model_insights_config[model]:
            models.append(model.InsightsModel)

    with ModelWriter(proxy.args.output, models) as writer:
        for model in models:
            if model is InsightsModel or issubclass(model, InsightsModel):
                # We only added this to the list so that ModelWriter knows we are about to export it
                continue
            config = model_config[model]
            logger.info("Listing %s (config: %s)", model.__name__, config or "default")
            for line, instance in client.list_by_model(model, params={**config.get("params", {}), "limit": limit}):
                writer.writerow(line)
                if model_insights_config[model]:
                    logger.info("Listing related insights for model")
                    date_preset = model_insights_config[model]["date_preset"]
                    time_increment = model_insights_config[model].get("time_increment", 1)
                    breakdown = model_insights_config[model].get("breakdown", [])
                    writer.writerows(
                        client.list_insights(
                            base=instance,
                            date_preset=date_preset,
                            time_increment=time_increment,
                            limit=limit,
                            breakdown=breakdown,
                        )
                    )


if __name__ == "__main__":
    main()
