FROM python:3.10-slim

RUN apt-get update && apt-get -y install curl

RUN curl -sSL https://install.python-poetry.org | python -

WORKDIR /code

ENV PATH=/root/.local/bin:$PATH
ENV POETRY_VIRTUALENVS_CREATE=false

ADD poetry.lock .
ADD pyproject.toml .

RUN poetry install --no-dev

ADD . .

ENTRYPOINT ["python", "-u", "main.py"]
